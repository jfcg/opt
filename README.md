## opt [![go report card](https://goreportcard.com/badge/github.com/jfcg/opt)](https://goreportcard.com/report/github.com/jfcg/opt) [![go.dev ref](https://pkg.go.dev/static/frontend/badge/badge.svg)](https://pkg.go.dev/github.com/jfcg/opt#pkg-overview)
`FindMin*()` optimization routines.
opt uses [semantic](https://semver.org) versioning.

### Support
If you use opt and like it, please support via:
- BTC:`bc1qr8m7n0w3xes6ckmau02s47a23e84umujej822e`
- ETH:`0x3a844321042D8f7c5BB2f7AB17e20273CA6277f6`
